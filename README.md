# Study Orchy 
This repo is for study orchy microfrontend framework.

## How to dev
* node version
```
source ~/.asdf/asdf.fish
```
* orchy backend (seams unecessary)
```
cd orchy
pnpm i github:orchy-mfe/orchy-core
pnpm run prepare
pnpm run orchy

# http://localhost:5173/
```
* mfe svelte
```
cd orchy
npm install create-orchy-mfe
npx create-orchy-mfe

cd svelte-mfe
pnpm i
pnpm run dev

# http://localhost:5000/
```
* orchy satic file
```
cd orchy
pnpm i fastify @fastify/static

pnpm dev
# http://localhost:3000/api/v1/configuration/page-config.html
```

# References
* pnpm
* svelte
* [orchy](https://orchy-mfe.github.io/docs/documentation/intro)
* fastify
