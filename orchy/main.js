const fastify = require('fastify')({
    logger: true
})

const fastifyStatic = require('@fastify/static')
const path = require('path')

fastify.register(require('@fastify/static'), {
    root: path.join(__dirname, 'orchy_static'),
    prefix: '/api/v1/configuration/',
})

// Declare a route
fastify.get('/', function (request, reply) {
    reply.send({ status: 'ok' })
})

// Run the server!
fastify.listen({ port: 3000 }, function (err, address) {
    if (err) {
        fastify.log.error(err)
        process.exit(1)
    }
})